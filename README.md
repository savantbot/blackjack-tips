# blackjack-tips
> My first Alexa Skill - Blackjack Tips

This was originally based on the Alexa "Fact Skill Template",
and I added some minor modifications.

The skill recites "blackjack tips", mainly playing tips based on the basic strategy table.
You can ask Alexa to "give me a tip" or "give me 5 tips", etc.


The primary motivation for creating this skill was to learn Alexa Skills Kit SDK (aka "ASK").
Since this skill, I've created a couple of dozen more Alexa skills, most of which are available in Amazon Alexa Skills Store.

* [My Published Alexa Skills](https://www.amazon.com/s/?url=search-alias%3Dalexa-skills&field-keywords=sideway+bot)


You can use this sample code to kick start your own Skills development. Or, if you want, you can use a Yeoman generator for alexa skills:

* [generator-alexa-skill-basic](https://www.npmjs.com/package/generator-alexa-skill-basic)





## Tutorial

* [Training for the Alexa Skills Kit](https://developer.amazon.com/alexa-skills-kit/alexa-skills-developer-training)
* [Alexa Skills Kit Fact Template: Step-by-Step Guide to Build a Fact Skill](https://developer.amazon.com/blogs/post/Tx3DVGG0K0TPUGQ/New-Alexa-Skills-Kit-Template:-Step-by-Step-Guide-to-Build-a-Fact-Skill)


