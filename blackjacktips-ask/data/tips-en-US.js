var data = [
  "An ace should generally be counted as 1 if the sum of all other cards in your hand is 6 or less. That is, a hand of ace and 5 should be viewed as 6 not 16.",
  "An ace should generally be counted as 11 if the sum of all other cards in your hand is 7 or more. That is, a hand of ace and 8 should be viewed as 19 not 9.",
  "You should always hit a hard hand of 8 or lower.",
  "You should generally hit a hard hand of 16 or lower if the dealer's upcard is 7 or higher.",
  "You can either hit or stand on a hard 16 or if the dealer's upcard is 10.",
  "You should generally stay on a hard hand of 13 or higher if the dealer's upcard is 6 or lower.",
  "You should generally hit 12 if the dealer's upcard is 2, 3, or 7 or higher.",
  "You should always hit or double down on a hard hand of 11 or lower.",
  "You should always hit a soft hand of 17 or lower.",
  "You should generally hit a soft 18 if the dealer's upcard is 9, 10, or ace.",
  "You should never hit a hard hand of 17 or higher.",
  "You should never stay on a hard hand of 11 or lower.",
  "You should never hit or double down on a soft hand of 19 or higher.",
  "You should always double down on 11.",
  "You should double down on 10 if the dealer's upcard is not 10 or ace.",
  "You should double down on 9 if the dealer's upcard is 3, 4, 5, or 6.",
  "You should double down on a soft 13 or a soft 14 if the dealer's upcard is 5, or 6.",
  "You should double down on a soft 15 or a soft 16 if the dealer's upcard is 4, 5, or 6.",
  "You should double down on a soft 17 if the dealer's upcard is 3, 4, 5, or 6.",
  "You should double down on a soft 18 if the dealer's upcard is 6 or lower.",
  "You should never double down on a hard hand of 12 or higher.",
  "You should never double down on a hard hand of 8 or lower.",
  "You should always split a pair of aces.",
  "You should always split a pair of tens.",
  "You should split a pair of nines if the dealer's upcard is 8 or 9, or 6 or lower.",
  "You should always split a pair of eights.",
  "You should split a pair of sevens if the dealer's upcard is 7 or lower.",
  "You should split a pair of sixes if the dealer's upcard is 3, 4, 5, or 6. You may split a sixes against the dealer's deuce if you can double down after splitting.",
  "You should never split a pair of fives.",
  "You may split a pair of fours if the dealer's upcard is 5 or 6 and if you can double down after spliting. Otherwise, Just hit.",
  "You should split a pair of threes if the dealer's upcard is 4, 5, 6, or 7. You may split a threes against the dealer's 2 or 3 if you can double down after splitting.",
  "You should split a pair of deuces if the dealer's upcard is 4, 5, 6, or 7. You may split a deuces against the dealer's 2 or 3 if you can double down after splitting."
];

var SKILL_NAME = "Blackjack Tips";
var GET_TIP_MESSAGE = "Here's your tip: ";
var HELP_MESSAGE = "You can say tell me a blackjack tip, or, you can say exit... What can I help you with?";
var HELP_REPROMPT = "What can I help you with?";
var STOP_MESSAGE = "Goodbye!";
