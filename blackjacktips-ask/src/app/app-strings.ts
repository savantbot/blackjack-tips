export var SKILL_NAME = "Blackjack Tips";
export var GET_TIP_MESSAGE = "Here's your tip: ";
// export var REPEAT_TIP_MESSAGE = "The tip was: ";
export var START_TIPS_PREFIX = "OK.  I will give you ";
export var START_TIPS_SUFFIX = " tips on blackjack play. ";
export var HELP_MESSAGE = "You can say tell me a blackjack tip, or give me 5 tips. Or, you can say exit... What can I help you with?";
export var HELP_REPROMPT = "What can I help you with?";
export var STOP_MESSAGE = " OK. Goodbye! ";
export var EXIT_SKILL_MESSAGE = `Thanks for using ${SKILL_NAME}. Goodbye! `;

