// TBD: Make these configurable? Or, Use input?
export const MAXIMUM_ITERATIONS = 25;
export const DEFAULT_MAX_TIPS = 5;
export const MIN_MAX_TIPS = 1;
export const MAX_MAX_TIPS = 20;
