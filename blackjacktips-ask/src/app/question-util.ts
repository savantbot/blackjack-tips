export function getOneMoreTipQuestion(): string {
  let question = "";
  let r = Math.floor(Math.random() * 15);
  switch (r) {
    case 0:
      question += " Another tip?";
      break;
    case 1:
      question += " One more tip?";
      break;
    case 2:
      question += " Do you want another tip?";
      break;
    case 3:
      question += " Do you want one more tip?";
      break;
    case 4:
      question += " Would you like another tip?";
      break;
    case 5:
      question += " Would you like one more tip?";
      break;
    case 6:
      question += " Do you want another blackjack tip?";
      break;
    case 7:
      question += " Do you want one more blackjack tip?";
      break;
    case 8:
      question += " Would you like another blackjack tip?";
      break;
    case 9:
      question += " Would you like one more blackjack tip?";
      break;
    default:
      question += " More?";
      break;
  }
  return question;
}
export function getMoreTipsQuestion(): string {
  let question = "";
  let r = Math.floor(Math.random() * 15);
  switch (r) {
    case 0:
      question += " More tips?";
      break;
    case 1:
      question += " Some more tips?";
      break;
    case 2:
      question += " Do you want more tips?";
      break;
    case 3:
      question += " Do you want some more tips?";
      break;
    case 4:
      question += " Would you like more tips?";
      break;
    case 5:
      question += " Would you like some more tips?";
      break;
    case 6:
      question += " Do you want more blackjack tips?";
      break;
    case 7:
      question += " Do you want some more blackjack tips?";
      break;
    case 8:
      question += " Would you like more blackjack tips?";
      break;
    case 9:
      question += " Would you like some more blackjack tips?";
      break;
    default:
      question += " More?";
      break;
  }
  return question;
}
