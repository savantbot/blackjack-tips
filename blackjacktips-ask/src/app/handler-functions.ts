import * as Alexa from 'alexa-sdk';
import { blackjackTips } from './blackjack-tips';
import { getOneMoreTipQuestion, getMoreTipsQuestion } from './question-util';
import * as AC from './app-constants';
import * as AS from './app-strings';


export function handleLaunchRequest(that: Alexa.Handler) {
  that.emit('GetNewTipIntent');
}
export function handleAmazonResumeIntent(that: Alexa.Handler) {
  that.emit('GetNewTipIntent');
}
export function handleAmazonStartOverIntent(that: Alexa.Handler) {
  that.emit('GetNewTipIntent');
}

export function handleGetNewTipIntent(that: Alexa.Handler) {
  that.attributes["isForMultipleTips"] = false;
  if (!that.attributes["counter"]) {
    that.attributes["counter"] = 0;
  }

  // TBD: Need to avoid giving the same tip as the previously given (immediately before this)
  let tipArr = blackjackTips;
  let tipIndex = Math.floor(Math.random() * tipArr.length);
  let randomTip = tipArr[tipIndex];
  // this.attributes['previousTip'] = randomTip;
  let intro = " ";
  if (that.attributes["counter"] == 0) {
    intro = AS.GET_TIP_MESSAGE;
  }
  let speechOutput = intro + randomTip;
  if (that.attributes["counter"]++ < AC.MAXIMUM_ITERATIONS) {
    let question = getOneMoreTipQuestion();
    let repromptQ = getOneMoreTipQuestion();
    let speechWithQuestion = speechOutput + " <break time='750ms'/> " + question;
    that.emit(":askWithCard", speechWithQuestion, repromptQ, AS.SKILL_NAME, randomTip);
  }
  else {
    let finalSpeech = speechOutput + AS.STOP_MESSAGE;
    that.emit(':tellWithCard', finalSpeech, AS.SKILL_NAME, randomTip);
  }
}

export function handleGetMultipleTipsIntent(that: Alexa.Handler) {
  that.attributes["isForMultipleTips"] = true;
  if (!that.attributes["counter"]) {
    that.attributes["counter"] = 0;
  }
  if (!that.attributes["maxTips"]) {
    that.attributes["maxTips"] = AC.DEFAULT_MAX_TIPS;
  }

  if (that.event && that.event.request && (that.event.request as any).intent && (that.event.request as any).intent.slots) {
    let tipCountSlot = (that.event.request as any).intent.slots.TipCount;
    if (tipCountSlot && tipCountSlot.value) {
      let iter = parseInt(tipCountSlot.value);
      if (iter && iter >= AC.MIN_MAX_TIPS && iter <= AC.MAX_MAX_TIPS) {
        that.attributes["maxTips"] = iter;
      } else if (iter && iter > AC.MAX_MAX_TIPS) {
        that.attributes["maxTips"] = AC.MAX_MAX_TIPS;
      } else if (iter && iter < AC.MIN_MAX_TIPS) {
        that.attributes["maxTips"] = AC.MIN_MAX_TIPS;
      } else {
        that.attributes["maxTips"] = AC.DEFAULT_MAX_TIPS;
      }
      console.log(`>>> iter = ${iter}; maxTips = ${that.attributes["maxTips"]}`);
    }
  }
  let tipCount = that.attributes["maxTips"];

  let intro = " ";
  // if (this.attributes["counter"] == 0) {
  intro = AS.START_TIPS_PREFIX + tipCount + AS.START_TIPS_SUFFIX;
  // }

  let tipCounter = 0;
  let loopCounter = 0;
  let indexMap: { [index: number]: number } = {};
  let phrase = " ";
  let allTips = "";
  let tipArr = blackjackTips;
  while (tipCounter < tipCount && loopCounter++ < 2 * tipCount + 10) {
    var tipIndex = Math.floor(Math.random() * tipArr.length);
    if (tipIndex in indexMap) {
      continue;
    }
    indexMap[tipIndex] = tipIndex;   // To avoid duplicates.
    var randomTip = tipArr[tipIndex];
    allTips += randomTip + '\n';
    // this.attributes['previousTip'] = randomTip;   // Keep only the last one.

    let segment = "Tip " + ++tipCounter + ": " + randomTip;
    phrase += segment + " <break time='750ms'/> ";
  }
  let speechOutput = intro + phrase;
  if (that.attributes["counter"]++ < AC.MAXIMUM_ITERATIONS) {
    let question = getMoreTipsQuestion();
    let repromptQ = getMoreTipsQuestion();
    let speechWithQuestion = speechOutput + " <break time='250ms'/> " + question;
    that.emit(":askWithCard", speechWithQuestion, repromptQ, AS.SKILL_NAME, allTips);
  }
  else {
    let finalSpeech = speechOutput + AS.STOP_MESSAGE;
    that.emit(':tellWithCard', finalSpeech, AS.SKILL_NAME, allTips);
  }
}


// For now, we only repeat the last tip even for multiple tips.
export function handleAmazonRepeatIntent(that: Alexa.Handler) {
  // if (this.attributes['previousTip']) {
  //   var previousTip = this.attributes['previousTip'];
  //   var speechOutput = REPEAT_TIP_MESSAGE + previousTip;
  //   this.emit(':tellWithCard', speechOutput, AS.SKILL_NAME, previousTip)
  // } else {
  //   this.emit('GetNewTipIntent');
  // }

  // Handlling repeat in the same way as "more". 
  if (that.attributes["isForMultipleTips"] && that.attributes["isForMultipleTips"] == true) {
    that.emitWithState("GetMultipleTipsIntent");
  } else {
    that.emitWithState("GetNewTipIntent");
  }
}

export function handleAmazonYesIntent(that: Alexa.Handler) {
  if (that.attributes["counter"] < AC.MAXIMUM_ITERATIONS) {
    if (that.attributes["isForMultipleTips"] && that.attributes["isForMultipleTips"] == true) {
      that.emitWithState("GetMultipleTipsIntent");
    } else {
      that.emitWithState("GetNewTipIntent");
    }
  }
  else {
    that.emit(":tell", AS.STOP_MESSAGE);
  }
}
export function handleAmazonNoIntent(that: Alexa.Handler) {
  that.emit(":tell", AS.STOP_MESSAGE);
}

export function handleAmazonHelpIntent(that: Alexa.Handler) {
  var speechOutput = AS.HELP_MESSAGE;
  var reprompt = AS.HELP_REPROMPT;
  that.emit(':ask', speechOutput, reprompt);
}
export function handleAmazonCancelIntent(that: Alexa.Handler) {
  that.emit(':tell', AS.STOP_MESSAGE);
}
export function handleAmazonStopIntent(that: Alexa.Handler) {
  that.emit(':tell', AS.STOP_MESSAGE);
}
export function handleUnhandled(that: Alexa.Handler) {
  that.emit(":tell", AS.EXIT_SKILL_MESSAGE);
}
