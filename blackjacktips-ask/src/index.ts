import * as Alexa from 'alexa-sdk';
import * as hf from './app/handler-functions';
import { APP_ID } from './app-id';


export default function handler(event, context, callback) {
  var alexa: Alexa.AlexaObject = Alexa.handler(event, context, callback);
  alexa.appId = APP_ID;
  alexa.registerHandlers(defaultHandlers);
  alexa.execute();
}

export var defaultHandlers: Alexa.Handlers = {
  'LaunchRequest': function () {
    let that = this;
    hf.handleLaunchRequest(that);
  },
  'AMAZON.ResumeIntent': function () {
    let that = this;
    hf.handleAmazonResumeIntent(that);
  },
  'AMAZON.StartOverIntent': function () {
    let that = this;
    hf.handleAmazonStartOverIntent(that);
  },

  'GetNewTipIntent': function () {
    let that = this;
    hf.handleGetNewTipIntent(that);
  },
  'GetMultipleTipsIntent': function () {
    let that = this;
    hf.handleGetMultipleTipsIntent(that);
  },
  'AMAZON.RepeatIntent': function () {
    let that = this;
    hf.handleAmazonRepeatIntent(that);
  },

  "AMAZON.YesIntent": function () {
    let that = this;
    hf.handleAmazonYesIntent(that);
  },
  "AMAZON.NoIntent": function () {
    let that = this;
    hf.handleAmazonNoIntent(that);
  },

  'AMAZON.HelpIntent': function () {
    let that = this;
    hf.handleAmazonHelpIntent(that);
  },
  'AMAZON.CancelIntent': function () {
    let that = this;
    hf.handleAmazonCancelIntent(that);
  },
  'AMAZON.StopIntent': function () {
    let that = this;
    hf.handleAmazonStopIntent(that);
  },
  "Unhandled": function () {
    let that = this;
    hf.handleUnhandled(that);
  }
};

